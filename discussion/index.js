// COMMENTS in JavaScript
// - Two write comments, we use two forward slash for single line comments and two forward slash and two asterisks for multi-line comments.
// This is a single line comment.
/*
	This is a multi-line comment.
	To write them, we ad two asterisks inside of the forward slashes and write comments in between them.
*/

// VARIABLE

/*

	- Variable contain values that can ne changes over the execution time of the program.
	- To declare a variable, we use the "let" keyword.
*/

let productName = 'desktop computer';
console.log(productName);
productName = 'cellphone'
console.log(productName);


let productPrice = 500;
console.log(productPrice);
productPrice = 450;
console.log(productPrice);

// CONSTANTS
/*
	- Use constants fo values that will not change.
*/

// const deliveryFee = 30;
// console.log(deliveryFee);

// DATA TYPES https://www.w3schools.com/js/js_datatypes.asp

// 1. STRINGS
/*

	- Strings are a series of characters that create a word, a phrase, sentence, or anything related to "TEXT"/
	- Strings in Javascript can be written using a single quote ('') or a double quote ("")
	- On other programming languages, only the double quote can be used for creating strings.

*/
let country = 'Philippines';
let province = "Metro Manila";

// CONCATENATION
console.log(country + ", " + province);

//  2. NUMBERS

/*
	- Include integers/whole numbers, decimal nubers, fractions, exponential notation
*/
let headCount = 26;
console.log(headCount);

let grade = 98.7;
console.log(grade)

let planetDistance = 2e10;
console.log(planetDistance);

let PI = 22/7;
console.log(PI);

console.log(Math.PI)

console.log("John's grade last quarter is: " + grade);

// 3. BOOLEAN
/*
	- Boolean value are logical values.
	- Boolean values can contain either "true" or "false"

*/
let isMarried = false;
let isGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("isGoodCOnduct: " + isGoodConduct);
console.log(isMarried);
console.log(isGoodConduct);

// 4. OBJECTS

	// Arrays
	/*
	
	- They are a special kind of data that stores multiple values.
	- They are a special type of an object.
	- They can store different data types but is normally used to store data types.
	- They are set of values.
	*/

	// Syntax:
		// let/const arrayName = [ElementA, ElementB, ElementC...]
	let grades = [ 98.7, 92.1, 90.2, 94.6 ];
	console.log(grades)
	console.log(grades[0])

	let userDetails = ["John", "Smith", 32, true]
	console.log(userDetails);

	// Object Leterals
	/*

	- Objects are another special kind of data type that mimics real world objects/items
	- They are used to create complex data that contains pieces of information that are relevant to each other.
	- Every individuals piece of information if called a property of the object

	Syntax:

		let/const objectName = {
			propertA: value,
			propertB: value,
		}


	*/

	let personDetails = {
		fullName: 'Juan Dela Cruz',
		age: 35, 
		isMarried: false,
		contact: ['+639876543210', '024785425'],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
	console.log(personDetails)


	// 5. NULL
	/*

		- It is used to intentionally express the absence of a value.
		means walang naman or indefine.
		or nag,aassign lang tayo ng absence of a value.
	*/

	let spouse = null

// 6. UNDEFINED

/*
	-Represents of the state of a variable that has been declared but without an assigned value.
*/

let vacationPlace:
console.log(vacationPlace)
